#!/usr/bin/env python3
import sys
import os
from sys import exit

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printHelp():
    print(f"{bcolors.HEADER}")
    print('NAME')
    print('    confedi - configuration helper written in Python')
    print('')
    print('SYNOPSIS')
    print('    confedi <filename> <operation> [block] [option] [value] [bash command]')
    print('')
    print('DESCRIPTION')
    print('    Confedi is designed to help with changing configuration files with one command. It offers finding/adding/deleting of blocks and options. It can also execute custom bash commands on configuration files.')
    print('')
    print('CONFEDI OPERATIONS')
    print('    --help ')
    print('        Shows this text.')
    print('')
    print('    --listblocks')
    print('        Lists blocks in targeted configuration file.')
    print('')
    print('    --printblock <block> ')
    print('        Shows all options in specified block.')
    print('')
    print('    --find <option> ')
    print('        Finds all occurrences of <option>.')
    print('')
    print('    --findvalue <value> ')
    print('        Searches all blocks for options set to specified value.')
    print('')
    print('    --addblock <block> ')
    print('        Creates a new block.')
    print('')
    print('    --add <block> <option> <value> ')
    print('        Adds option to block set to <value>.')
    print('')
    print('    --deleteblock <block> ')
    print('        Finds and deletes whole block.')
    print('')
    print('    --delete <block> <option> ')
    print('        Deletes option from choosen block.')
    print('')
    print('    --bash <block> <option> <bash command> ')
    print('        Executes the bash command and its output revrites value of a choosen option.')
    print(f"{bcolors.ENDC}")

def findBlock(lines, block):
    start, end = len(lines)+1, 0
    for i, line in enumerate(lines):
        line = line.rstrip()
        if line == block:
            start = i
            break

    for end, line in enumerate(lines[start+1:]):
        line = line.strip()
        if len(line) < 2 or (line[0] == '[' and line[-1] == ']'):
            break

    if start > len(lines):
        print(f"{bcolors.WARNING}Block not found.{bcolors.ENDC}")
        exit()
    
    if start + end == len(lines) - 2:
        end = end + 1

    return start, start + end + 1

def findValue(lines, block, name):
    start, end = findBlock(lines, block)
    found = False
    line_nr = 0
    for line_number, line in enumerate(lines[start:end]):
        if line.split('=')[0].strip() == name:
            found = True
            line_nr = line_number
            break

    return found, start+line_nr

def findOptionsSetToValueInBlock(lines, block, value):
    start, end = findBlock(lines, block)
    found = []
    for line_number, line in enumerate(lines[start:end]):
        if '=' in line:
            if line[line.index('=') + 1:].strip() == value:
                found.append(start+line_number)
    return found

def findAllOptionsSetToValue(lines, value):
    for block in getListOfBlocks(lines):
        items = findOptionsSetToValueInBlock(lines, block, value)
        if len(items) == 0:
            continue
        print(f"{bcolors.HEADER}" + f"{bcolors.BOLD}" + block + f"{bcolors.ENDC}")
        for item in items:
            print(lines[item])


def printBlock(f, lines, block):
    start, end = findBlock(lines, block)
    print(f"{bcolors.HEADER}" + f"{bcolors.BOLD}" + lines[start] + f"{bcolors.ENDC}", end = '')
    for line in lines[start + 1:end]:
        if line[0] == '#':
            line = f"{bcolors.OKGREEN}" + line + f"{bcolors.ENDC}"
        print(line, end = '')

def addBlock(f, lines, name):
    lines.append(name + "\n")
    f.write(''.join(lines))

def addValue(f, lines, block, name, value):
    found, line_number = findValue(lines, block, name)
    if found:
        lines[line_number] = lines[line_number].split("=")[0].strip() + "=" + value + "\n"
    else:
        lines.insert(line_number+1, name + "=" + value + "\n")
    f.write(''.join(lines))

def getListOfBlocks(lines):
    blocks = []
    for line in lines:
        if line[0] == '[' and line[-2] == ']':
            blocks.append(line.strip())
    return blocks

def listBlocks(f, lines):
    for block in getListOfBlocks(lines):
        print(f"{bcolors.HEADER}" + block + f"{bcolors.ENDC}")

def deleteBlock(f, lines, block):
    start, end = findBlock(lines, block)
    lines = lines[:start] + lines[end:]
    f.write(''.join(lines))
    f.truncate()

def deleteValue(f, lines, block, name):
    found, line_number = findValue(lines, block, name)
    if found == False:
        print(f"{bcolors.WARNING}Value not found.{bcolors.ENDC}")
        exit()
    lines = lines[:line_number] + lines[line_number + 1:]
    f.write(''.join(lines))
    f.truncate()

def bash(f, lines, block, name, command):
    found, line_number = findValue(lines, block, name)
    if found == False:
        print(f"{bcolors.WARNING}Value not found.{bcolors.ENDC}")
        exit()
    line = lines[line_number]
    value = line[line.find("=")+1:]
    stream = os.popen("echo \"" + value + "\" | " + command)
    output = stream.read()
    lines[line_number] = line.split("=")[0].strip() + "=" + output
    f.write(''.join(lines))
    f.truncate()

def findAllValues(lines, name):
    for block in getListOfBlocks(lines):
        found, line_number = findValue(lines, block, name)
        if found:
            print(f"{bcolors.HEADER}" + f"{bcolors.BOLD}" + block + f"{bcolors.ENDC}") 
            print(lines[line_number])

def check_argv(number_of_args):
    if len(sys.argv) != number_of_args:
        print(f"{bcolors.WARNING}Wrong number of arguments.{bcolors.ENDC}")
        exit()

if (len(sys.argv) < 3):
    printHelp()
    exit()
else:
    dir = sys.argv[1]
    try:
        f = open(dir, "r+")
    except FileNotFoundError:
        if sys.argv[1] == '--help':
            printHelp()
        else:
            print(f"{bcolors.WARNING}File not found.{bcolors.ENDC}")
        exit()

lines = f.readlines()
f.seek(0)


mode  = 'WARNING - NoModeGiven'
block = 'WARNING - NoBlockNameGiven'
name  = 'WARNING - NoOptionNameGiven'
value = 'WARNING - NoOptionValueGiven'

if (len(sys.argv) > 2): mode  =       sys.argv[2]
if (len(sys.argv) > 3): block = '[' + sys.argv[3] + ']'
if (len(sys.argv) > 4): name  =       sys.argv[4]
if (len(sys.argv) > 5): value =       sys.argv[5]

if mode == "--help":
    printHelp()

elif mode == "--listblocks":
    listBlocks(f, lines)

elif mode == "--printblock":
    check_argv(4)
    printBlock(f, lines, block)

elif mode == "--find":
    check_argv(4)
    findAllValues(lines, block[1:-1])

elif mode == "--addblock":
    check_argv(4)
    addBlock(f, lines, block)

elif mode == "--add":
    check_argv(6)
    addValue(f, lines, block, name, value)

elif mode == "--deleteblock":
    check_argv(4)
    deleteBlock(f, lines, block)

elif mode == "--delete":
    check_argv(5)
    deleteValue(f, lines, block, name)

elif mode == "--bash":
    check_argv(6)
    command = sys.argv[5]
    bash(f, lines, block, name, command)

elif mode == "--findvalue":
    check_argv(4)
    findAllOptionsSetToValue(lines, block[1:-1])

else:
    print(f"{bcolors.WARNING}Command not found. Try confedi --help.{bcolors.ENDC}")

