.\" Manpage for confedi.

.TH man 8 "25 Jan 2021" "1.0" "confedi man page"

.SH NAME
    confedi - configuration helper written in Python

.SH SYNOPSIS
    confedi <filename> <operation> [block] [option] [value] [bash command]

.SH DESCRIPTION
    Confedi is designed to help with changing configuration files with one command. It offers finding/adding/deleting of blocks and options. It can also execute custom bash commands on configuration files.

.SH CONFEDI OPERATIONS
    --help 
        Shows this text.

    --listblocks
        Lists blocks in targeted configuration file.

    --printblock <block> 
        Shows all options in specified block.

    --find <option> 
        Finds all occurrences of <option>.

    --findvalue <value> 
        Searches all blocks for options set to specified value.

    --addblock <block> 
        Creates a new block.

    --add <block> <option> <value> 
        Adds option to block set to <value>.

    --deleteblock <block> 
        Finds and deletes whole block.

    --delete <block> <option> 
        Deletes option from choosen block.

    --bash <block> <option> <bash command> 
        Executes the bash command and its output revrites value of a choosen option.